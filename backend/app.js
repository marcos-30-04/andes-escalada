const express = require("express");
const cors = require("cors");
const mongoose = require("mongoose");
const cookieParser = require("cookie-parser");

require("dotenv").config();

const app = express();
const port = process.env.PORT || 5000;

// Bodyparser
app.use(express.urlencoded({ extended: true }));

app.use(
  cors({
    origin: "http://localhost:3000", // <-- location of the react app were connecting to
    credentials: true,
  })
);

app.get("/", (req, res) => {
  res.send(`Welcome to "andes-escalada"`);
});
app.listen(port, () => {
  console.log(`Server is running on port: ${port}`);
});
